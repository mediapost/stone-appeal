<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stoneappeal');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@~^sA6fX}n@k/$Y~,aWXdw26]EZ1}m0XrQGte?y4x^%>~JR)Lexqk(k*{]n:1t9b');
define('SECURE_AUTH_KEY',  'r@i74@esdd$xl=Z2YYB0hsL|*UjA3eA>Ix~eG&V]n?SjFC yO>C*D9`p9[^]aLZ!');
define('LOGGED_IN_KEY',    '$nkYh0d;38A.WM:b^!e>gqR8PQ)&Wb=Rtf7.`rqeIxa$}#G/c{~9/b^l95{;~Z6F');
define('NONCE_KEY',        '>_vd@[|E:J_&YxTu|S[9y=j{DSik}eD2]}z8`6^}_WB+L^bo6jH@V{(Cs,!p.tYD');
define('AUTH_SALT',        '}|)8;C+ik$2es^rN3:28Vp[nB(R{|{topOan*7[?@6}jZu=q2=0}fC]dT`Q;e>h@');
define('SECURE_AUTH_SALT', 'p=Q|6#XgV_qt_V$h)`Q:D#r||UyFR_X4CP}r>>a.JS* O#=CjR[TUt}1k;Yjep Y');
define('LOGGED_IN_SALT',   'TbfAFqfofd~CQ20bfNDY+[Fw.6n9Yn@!Q!/p7lWq3lEzOBOyiV8K<-wC7LSWZVsj');
define('NONCE_SALT',       'n}mG$8e{mXiQ,`D|cJ8ii!+4+hIup>G+/k^~a~*68pd,2,{7m3oqQ3>cOoDY=Y]<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_MEMORY_LIMIT', '256M');
