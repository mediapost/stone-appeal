// init Masonry
var jQuerygrid = jQuery('.grid').masonry({
  itemSelector: '.grid-item',
  percentPosition: true,
  columnWidth: '.grid-sizer',
});


jQuery(document).ready(function() {

  jQuery(".grid-item").hover(function() {
    console.log(jQuery(this).find('img').attr("alt"));
  });

  jQuery('.grid-item').find('img').on("click", function() {
    var imgSrc = jQuery(this).attr("src");
    var imgAlt = 'tetet';
    jQuery('#page-mask').addClass('show');
    console.log(imgSrc);
    jQuery('.img-box').addClass('show');
    jQuery('#img-box').addClass('show').html('<img src="' + imgSrc + '"/><div class="img-detail">' + imgAlt + '</div>');
  });

  jQuery('.close-btn').click(function() {
    jQuery('.img-box').removeClass('show');
    jQuery('#page-mask').removeClass('show');
  })

});
