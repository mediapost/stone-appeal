jQuery('.diamond-group').hover(function() {
  jQuery(this).children('.diamond-overlay').addClass('fadeIn');
  jQuery(this).children('.diamond-text').addClass('fadeIn');
}, function() {
  jQuery(this).children('.diamond-overlay').removeClass('fadeIn');
  jQuery(this).children('.diamond-text').removeClass('fadeIn');
});

jQuery('.mobile-menu-btn').click(function() {
  jQuery('.mobile-slide-out').addClass('show');
});

jQuery('.mobile-slide-out .close-btn').click(function() {
  jQuery('.mobile-slide-out').removeClass('show');
});
