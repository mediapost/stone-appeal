jQuery(function($){
  $.fn.extend({
    beforeAfter: function(options){

      var defaults = {
        direction : "h",
        returnToCenter : 'true',
        start: 'center',
        onReady: function(){}
      };
      var options = $.extend(defaults, options);
      var barWidth = jQuery('.before-after-bar').width();

      var container = jQuery(this);
      var backImage = jQuery('.backImage');
      var width = jQuery('.frontImage img').width();
      var height = jQuery('.frontImage img').height();

      backImage.children().width(width);
      backImage.children().height(height);


      var bar = jQuery('.before-after-bar');
      var centerX = width/2;
      var centerY = height/2;

      bar.css('height', jQuery('.frontImage img').height());
      bar.css('left', jQuery('.frontImage img').width()/2-(barWidth/2));
      backImage.width(jQuery('.frontImage img').width()/2);

      container.on( "mousemove", function( event ) {

        var screenX = container.position().left;
        var screenY = container.position().top;

        var mouseX = event.pageX;
        var mouseY = event.pageY;

        var newX = mouseX-screenX;
        if(newX > width){
          newX = width;
        }
        if(newX < 0){
          newX = 0;
        }
        bar.css('left', newX-(barWidth/2));
        backImage.width(newX);
      });
      container.on('mouseleave', function(event){
        bar.animate({'left' : (jQuery('.frontImage img').width()/2)-(barWidth/2)}, 300);
        backImage.animate({'width': jQuery('.frontImage img').width()/2}, 300);
      });
    }
  })
});

jQuery(function(){
  var beforeImg = document.getElementById('img1');
  var afterImg = document.getElementById('img2');

  jQuery('.images-list ul li').on("click", function() {
    var selected = jQuery(this).children('div');
    console.log(selected.data("after"));
    jQuery('#img2').attr('src', selected.data("before"));
    jQuery('#img1').attr('src', selected.data("after"));
    jQuery('.backImage').width(jQuery('.frontImage img').width()/2);
    jQuery('.backImage img').css('width',jQuery('.frontImage img').css('width'));
    jQuery('.backImage img').css('height',jQuery('.frontImage img').css('height'));
    jQuery('.before-after-bar').css('height',jQuery('.frontImage img').height());
    jQuery('.before-after-bar').css('left', jQuery('.frontImage img').width()/2-(jQuery('.before-after-bar').width()/2));
  })

  jQuery('#container').beforeAfter();
});

// Update sliders on resize.
jQuery(window).resize(function(){
	jQuery('.backImage').width(jQuery('.frontImage img').width()/2);
  jQuery('.backImage img').css('width',jQuery('.frontImage img').css('width'));
  jQuery('.backImage img').css('height',jQuery('.frontImage img').css('height'));
  jQuery('.before-after-bar').css('height',jQuery('.frontImage img').height());
  jQuery('.before-after-bar').css('left', jQuery('.frontImage img').width()/2-(jQuery('.before-after-bar').width()/2));
});
