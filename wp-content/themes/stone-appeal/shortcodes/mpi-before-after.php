<?php ob_start(); ?>

<style>
  .wrapper {
    max-width: 1280px;
    margin: 0 auto;
  }

  #container {
    position: relative;
    width: 100%;
  }
  .before-after-bar {
    position: absolute;
    top: 0;
    background-color: #fff !important;
    width: 3px;
    background-size: 100% 100%;
    z-index: 3;
  }
  .before-after-bar:before {
    content: '';
    position: absolute;
    left: 0; margin-left: -9px;
    top: 50%; margin-top: -9px;
    display: block;
    width: 20px;
    height: 20px;
    background-color: #fff;
    transform: rotate(45deg);
  }
  .frontImage {
    overflow: hidden;
    z-index: 2;
  }
  .backImage {
    position: absolute;
    top: 0;
    overflow: hidden;
    z-index: 1;
  }

  .images-list ul {
    list-style: none;
    margin: 0;
    padding: 0;
    text-align: center;
  }

  .images-list ul li {
    display: inline-block;
  }

  .image-list-container {
    width: 125px;
    height: 125px;
    margin-top: 10px;
    margin-right: 10px;
    display: inline-block;
    cursor: pointer;
  }

  @media screen and (max-width: 1000px) {
    .image-list-container {
      width: 80px;
      height: 80px;
      margin-top: 10px;
      margin-right: 10px;
      display: inline-block;
    }
  }
</style>

<div class="wrapper">
<div id="container" class="clear">
  <?php
  $main_field = get_field('before_after');
  $second_img = $main_field[0]['before'];
  $first_img = $main_field[0]['after'];
  ?>
  <div class="frontImage"><img id="img1" class="img1" alt="before" src="<?php echo $first_img ?>" width="100%" height="auto" /></div>
  <div class="backImage"><img id="img2" class="img2" alt="after" src="<?php echo $second_img ?>" width="100%" height="auto" /></div>
  <span class="before-after-bar"></span>

</div>

    <div class="images-list">
      <ul>
        <?php

        if( have_rows('before_after') ):

         	// loop through the rows of data
            while ( have_rows('before_after') ) : the_row();

        ?>
        <li><div class="image-list-container" data-before="<?php the_sub_field('before')?>" data-after="<?php the_sub_field('after')?>" style="background: url(<?php the_sub_field('before')?>) no-repeat center center/cover"></div></li>
        <?php
            endwhile;

        else :

            // no rows found

        endif;

        ?>
      </ul>
</div>

</div>
<?php wp_reset_postdata(); return ob_get_clean(); ?>
