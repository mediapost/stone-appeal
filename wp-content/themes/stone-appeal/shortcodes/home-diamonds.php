<svg version="1.1" id="DiamondSVG" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 1280 700" style="enable-background:new 0 0 1280 700;" xml:space="preserve">
   <defs>
     <!-- Diamond 1 Background -->
    <pattern id="diamond1" height="100%" width="100%" patternContentUnits="objectBoundingBox">
        <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_1') ?>" />
    </pattern>
     <!-- Diamond 2 Background -->
     <pattern id="diamond2" height="100%" width="100%" patternContentUnits="objectBoundingBox">
        <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_2') ?>" />
    </pattern>
     <!-- Diamond 3 Background -->
     <pattern id="diamond3" height="100%" width="100%" patternContentUnits="objectBoundingBox">
        <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_3') ?>" />
    </pattern>
     <!-- Diamond 4 Background -->
     <pattern id="diamond4" height="100%" width="100%" patternContentUnits="objectBoundingBox">
        <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_4') ?>" />
    </pattern>
    <pattern id="diamond5" height="100%" width="100%" patternContentUnits="objectBoundingBox">
       <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_5') ?>" />
    </pattern>
    <pattern id="diamond6" height="100%" width="100%" patternContentUnits="objectBoundingBox">
      <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_6') ?>" />
    </pattern>
    <pattern id="diamond7" height="100%" width="100%" patternContentUnits="objectBoundingBox">
     <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_7') ?>" />
    </pattern>
	<pattern id="diamond8" height="100%" width="100%" patternContentUnits="objectBoundingBox">
     <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_8') ?>" />
    </pattern>
	<pattern id="diamond9" height="100%" width="100%" patternContentUnits="objectBoundingBox">
     <image height="1" width="1" preserveAspectRatio="xMidYMid slice" xlink:href="<?php the_field('diamond_9') ?>" />
    </pattern>
    </defs>
    <a href="<?php the_field('diamond_1_link') ?>"><g class="diamond-group diamond-group-1">
    <polygon class="diamond-1 diamond" fill="url('#diamond1')" points="260.1,131 131.6,259.6 3,131 131.6,2.4 "/>
    <polygon class="diamond-overlay" points="260.1,131 131.6,259.6 3,131 131.6,2.4 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="130" y="140"><?php the_field('diamond_1_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_2_link') ?>"><g class="diamond-group diamond-group-2">
    <polygon class="diamond-2 diamond" fill="url('#diamond2')" points="586.3,269.2 391.7,463.8 197.1,269.2 391.7,74.5 "/>
    <polygon class="diamond-overlay" points="586.3,269.2 391.7,463.8 197.1,269.2 391.7,74.5 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="385" y="275"><?php the_field('diamond_2_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_3_link') ?>"><g class="diamond-group diamond-group-3">
    <polygon class="st0" fill="url('#diamond3')" points="367,520 201.7,685.2 36.5,520 201.7,354.7 "/>
    <polygon class="diamond-overlay" points="367,520 201.7,685.2 36.5,520 201.7,354.7 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="200" y="520"><?php the_field('diamond_3_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_4_link') ?>"><g class="diamond-group diamond-group-4">
    <polygon class="st0" fill="url('#diamond4')" points="1099.2,291.4 888.1,502.5 677,291.4 888.1,80.3 "/>
    <polygon class="diamond-overlay" points="1099.2,291.4 888.1,502.5 677,291.4 888.1,80.3 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="890" y="300"><?php the_field('diamond_4_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_5_link') ?>"><g class="diamond-group diamond-group-5">
    <polygon class="st0" fill="url('#diamond5')" points="784,476.4 617.9,642.5 451.7,476.4 617.9,310.2 "/>
    <polygon class="diamond-overlay" points="784,476.4 617.9,642.5 451.7,476.4 617.9,310.2 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="620" y="480"><?php the_field('diamond_5_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_6_link') ?>"><g class="diamond-group diamond-group-6">
    <polygon class="st0" fill="url('#diamond6')" points="1277.5,131.6 1149.1,260 1020.7,131.6 1149.1,3.2 "/>
    <polygon class="diamond-overlay" points="1277.5,131.6 1149.1,260 1020.7,131.6 1149.1,3.2 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="1150" y="135"><?php the_field('diamond_6_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_7_link') ?>"><g class="diamond-group diamond-group-7">
    <polygon class="st0" fill="url('#diamond7')" points="1266,496.9 1118.3,644.6 970.6,496.9 1118.3,349.1 "/>
    <polygon class="diamond-overlay" points="1266,496.9 1118.3,644.6 970.6,496.9 1118.3,349.1 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="1120" y="500"><?php the_field('diamond_7_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_8_link') ?>"><g class="diamond-group diamond-group-8">
    <polygon class="st0" fill="url('#diamond8')" points="911.3,606.1 817.4,700 723.5,606.1 817.4,512.2 "/>
    <polygon class="diamond-overlay" points="911.3,606.1 817.4,700 723.5,606.1 817.4,512.2 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="820" y="610"><?php the_field('diamond_8_link_text') ?></text>
    </g></a>
    <a href="<?php the_field('diamond_9_link') ?>"><g class="diamond-group diamond-group-9">
    <polygon class="st0" fill="url('#diamond9')" points="740.4,151.4 644.2,247.5 548.1,151.4 644.2,55.2 "/>
    <polygon class="diamond-overlay" points="740.4,151.4 644.2,247.5 548.1,151.4 644.2,55.2 "/>
    <text class="diamond-text pointer" text-anchor="middle"
        x="640" y="160"><?php the_field('diamond_9_link_text') ?></text>
    </g></a>
</svg>
