<?php ob_start(); ?>

<style>
  .product-wrapper {
    overflow: hidden;
    padding: 10px;
  }

  .product-container {
    overflow: hidden;
  }

  .product {
    height: 200px;
    padding: 0;
    transition: 0.3s;
  }

  .caption {
    width: 100%;
    height: 100%;
    background: rgba(89,89,43,0.8);
    display: flex;
    justify-content: center;
    align-items: center;
    opacity: 0;
    transition: 0.3s;
  }

  .caption p {
    font-size: 16px;
    color: #fff;
    text-align: center;
  }

  .product:hover {
    transform: scale(1.1);
    transition: 0.3s;
  }

  .product:hover .caption {
    opacity: 1;
    transition: 0.3s;
  }
</style>

<div class="row">

  <?php

  if( have_rows('products') ):

    // loop through the rows of data
      while ( have_rows('products') ) : the_row();

  ?>
  <div class="col-md-4 col-sm-6 col-xs-12 product-wrapper">
    <div class="product-container">
      <div class="product" style="background: url(<?php the_sub_field('product_image')?>) no-repeat center center/cover">
        <div class="caption">
          <p><?php the_sub_field('product_title')?></p>
        </div>
      </div>
    </div>
  </div>

  <?php
      endwhile;

  else :

      // no rows found

  endif;

  ?>

</div>

<?php wp_reset_postdata(); return ob_get_clean(); ?>
