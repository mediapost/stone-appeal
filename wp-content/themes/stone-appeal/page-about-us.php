<?php
/* Template Name: About Us Page Template */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stone_Appeal
 */

get_header('inner'); ?>

	<div id="primary" class="content-area" style="background: #fff;">
		<main id="main" class="site-main">
			<div class="content-wrapper">
				<div class="row">
					<?php
					while ( have_posts() ) : the_post();
					?>

					<div class="col-md-8 col-md-offset-2 col-xs-12">
						<div class="inner-content-heading">
							<?php	the_field('content_heading'); ?>
						</div>
					</div>

					<div class="col-md-8 col-md-offset-2 col-xs-12">
						<div class="inner-content-box">
							<?php	the_field('content_box'); ?>
						</div>
					</div>

					<div class="col-md-8 col-md-offset-2 col-xs-12">
						<?php the_content(); ?>
					</div>

					<?php
					endwhile; // End of the loop.
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
