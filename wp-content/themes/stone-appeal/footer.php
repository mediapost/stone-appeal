<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stone_Appeal
 */

?>

	</div><!-- #content -->

	<?php

	if (!is_front_page()) {

	?>

	<footer id="colophon" class="site-footer">
		<div class="row footer-row">

				<div class="col-md-3 col-xs-12">
					<a href="<?php echo get_option("siteurl"); ?>"><?php the_custom_logo(); ?></a>
				</div>
				<div class="col-md-3 col-xs-6">
					<?php
						if(is_active_sidebar('footer-column-1')){
						dynamic_sidebar('footer-column-1');
						}
					?>
				</div>
				<div class="col-md-3 col-xs-6">
					<?php
						if(is_active_sidebar('footer-column-2')){
						dynamic_sidebar('footer-column-2');
						}
					?>
				</div>
				<div class="col-md-3 col-xs-6">
					<?php
						if(is_active_sidebar('footer-column-3')){
						dynamic_sidebar('footer-column-3');
						}
					?>
				</div>

		</div>
	</footer><!-- #colophon -->

	<?php

	}

	?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
