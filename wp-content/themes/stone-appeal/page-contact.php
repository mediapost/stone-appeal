<?php
/* Template Name: Contact Page Template */
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Stone_Appeal
 */

get_header('inner'); ?>

	<div id="primary" class="content-area" style="background: #fff;">
		<main id="main" class="site-main">
			<div class="content-wrapper">
				<div class="row">
					<?php
					while ( have_posts() ) : the_post();
					?>

					<div class="col-md-8 col-md-offset-2 col-xs-12">
						<div class="inner-content-heading">
							<?php	the_field('content_heading'); ?>
						</div>
					</div>

					<div class="col-md-12 col-xs-12">
						<?php the_content(); ?>
					</div>

					<div class="col-md-8 col-md-offset-2 col-xs-12">
						<div class="row">
							<div class="col-md-4" style="text-align: center;">
								<p>Stone and Epoxy Concrete Resurfacing</p>
								<p>Proudly Serving OH, PA</p>
							</div>

							<div class="col-md-4" style="text-align: center;">
								<p>877-295-4545</p>
								<p>mystoneappeal@yahoo.com</p>
							</div>

							<div class="col-md-4" style="text-align: center;">
								<p>HICPA Reg. 053260</p>
								<p>Free consultations</p>
							</div>
						</div>
					</div>
					<?php
					endwhile; // End of the loop.
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
