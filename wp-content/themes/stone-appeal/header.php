<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Stone_Appeal
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/0.9.3/hamburgers.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://use.typekit.net/pug0hob.css">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header class="home-header">

  <nav>
    <div class="nav-container row middle-xs">
      <div class="logo col-md-2 col-xs-9">
        <a href="<?php echo get_option("siteurl"); ?>"><?php the_custom_logo(); ?></a>
      </div>
      <div class="col-md-10 col-xs-3 nav-col">
        <div class="mobile-menu-btn">
          <button class="hamburger hamburger--stand" type="button">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button>
        </div>
        <?php wp_nav_menu(array('menu' => 'Primary', 'menu_class' => 'full-menu', 'container' => '')); ?>
      </div>

      <div class="mobile-slide-out">
        <div class="close-btn pointer">&times</div>
      	<?php wp_nav_menu(array('menu' => 'Primary', 'menu_class' => 'mobile-menu', 'container' => '')); ?>
      </div> <!-- /mobile-slide-out -->

    </div>
  </nav>

</header>

	<div id="content" class="site-content" style="background: #1f2328">
