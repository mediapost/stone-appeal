<?php
/**
 * Stone Appeal functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Stone_Appeal
 */

if ( ! function_exists( 'stone_appeal_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function stone_appeal_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Stone Appeal, use a find and replace
		 * to change 'stone-appeal' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'stone-appeal', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'stone-appeal' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'stone_appeal_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'stone_appeal_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function stone_appeal_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'stone_appeal_content_width', 640 );
}
add_action( 'after_setup_theme', 'stone_appeal_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function stone_appeal_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'stone-appeal' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'stone-appeal' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'stone_appeal_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function stone_appeal_scripts() {

	wp_enqueue_style( 'stone-appeal-grid', get_template_directory_uri() . '/inc/flexboxgrid.css' );

	wp_enqueue_style( 'stone-appeal-style', get_stylesheet_uri() );

	wp_enqueue_script( 'stone-appeal-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'stone-appeal-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'stone-appeal-menu', get_template_directory_uri() . '/js/menu.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'stone-appeal-masonry-grid', get_template_directory_uri() . '/js/masonry-grid.js', array(), '20151215', true );

	wp_enqueue_script( 'stone-appeal-masonry', get_template_directory_uri() . '/js/masonry.min.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'stone-appeal-mpi-masonry', get_template_directory_uri() . '/js/mpi-masonry.js', array('stone-appeal-masonry'), '20151215', true );

	wp_enqueue_script( 'stone-appeal-before-after', get_template_directory_uri() . '/js/before-after.js', array('jquery'), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'stone_appeal_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

register_sidebar( array(
'name' => 'Footer Column 1',
'id' => 'footer-column-1',
'description' => 'Appears in the footer area',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
register_sidebar( array(
'name' => 'Footer Column 2',
'id' => 'footer-column-2',
'description' => 'Appears in the footer area',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
register_sidebar( array(
'name' => 'Footer Column 3',
'id' => 'footer-column-3',
'description' => 'Appears in the footer area',
'before_widget' => '<div>',
'after_widget' => '</div>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );

// Homepage diamonds shortcode
function mpi_home_diamonds( $atts ) {
    include('shortcodes/home-diamonds.php');
}
add_shortcode( 'mpi_home_diamonds', 'mpi_home_diamonds');

// Before/after gallery shortcode
function mpi_before_after( $atts ) {
    return include('shortcodes/mpi-before-after.php');
}
add_shortcode( 'mpi_before_after', 'mpi_before_after');

// Masonry Image gallery shortcode
function mpi_masonry( $atts ) {
    return include('shortcodes/mpi-masonry.php');
}
add_shortcode( 'mpi_masonry', 'mpi_masonry');

// Product Image shortcode
function mpi_products( $atts ) {
    return include('shortcodes/mpi-products.php');
}
add_shortcode( 'mpi_products', 'mpi_products');
